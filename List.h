#include <iostream>
#include <cstdlib>

class List {
private:
    int* pointer;
    int current;
    int size;

    void expand(int newSize);
public:
    List(int size=10);

    int length() const;

    void report() const;
    void print() const;
    
    // TODO: zeroFill() const;
    void zeroFill();
    void clear();
    
    int count(int element) const;

    void add(int value, int amount);

    ~List();
};

std::ostream& operator<<(std::ostream os, const List& lst);