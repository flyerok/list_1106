#include <iostream>
#include <cstdlib>
#include "List.h"

List::List(int size) {
    this->pointer = (int*)malloc(sizeof(int) * size);
    this->current = 0;
    this->size = size;

    if ( DEBUG ) {
        std::cout << "List::List(int size)" << std::endl;
    }
}

void List::expand(int new_size) {
    int* temp;
    new_size = new_size + this->size / 10;
    
    if ( DEBUG ) {
        std::cout << "List::expand(int new_size)" << std::endl;
    }

    temp = (int*)realloc(this->pointer, sizeof(int) * new_size);

    if ( temp != NULL ) {
        this->pointer = temp;
        this->size = new_size;
    } else {
        // TODO: // how to notify user about error with C++
        std::cout << "Realloc is failed" << std::endl;
    }
}

void List::add(int value, int amount) {
    int new_size = this->current + amount;

    if ( new_size > this->size ) {
        this->expand(new_size);
    }

    for ( int i = 0; i < amount; i++ ) {
        this->pointer[this->current] = value;
        this->current += 1;
    }

    if ( DEBUG ) {
        std::cout << "List::add(int value, int amount)" << std::endl;
    }
}

List::~List() {
    if ( DEBUG ) {
        std::cout << "List::~List()" << std::endl;
    }
}
